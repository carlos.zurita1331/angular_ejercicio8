import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-practice',
  templateUrl: './practice.component.html',
  styleUrls: ['./practice.component.css']
})
export class PracticeComponent implements OnInit {

  valor1:number=0;
  a:number=1;
  mensaje:boolean=false;


  suma(){

    if (this.valor1<50){
      this.valor1=this.valor1+this.a;
      this.mensaje=false;
    } else {
      this.mensaje  = true;
    }
  }

  resta(){
    if (this.valor1>0){
      this.valor1=this.valor1-this.a;
      this.mensaje=false;
      
    } else {
      this.mensaje  = true;
      
    }
   }

  ngOnInit(): void {
  }

}
